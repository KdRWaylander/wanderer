﻿using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    [SerializeField] private float m_MinWanderTime = .5f;
    [SerializeField] private float m_MaxWanderTime = 3f;
    [SerializeField] private bool m_CanMove = true;

    private Vector3 m_TargetPosition;
    private float m_WanderTime;
    private float m_CurrentTime;

    public float Speed { get; set; }
    public int NumRays { get; set; }
    public float RayLength { get; set; }

    private void Start()
    {
        Speed = GameManager.Instance.Speed;
        NumRays = GameManager.Instance.Awareness;
        RayLength = GameManager.Instance.Proximity;

        m_TargetPosition = SetNewTarget();
        m_WanderTime = SetNewWanderTime();
        m_CurrentTime = 0;
    }

    private void Update()
    {
        m_CurrentTime += Time.deltaTime;
        if (m_CurrentTime >= m_WanderTime)
        {
            m_TargetPosition = SetNewTarget();
            m_WanderTime = SetNewWanderTime();
            m_CurrentTime = 0;
        }

        CheckSurroundings(NumRays, RayLength);

        if(m_CanMove)
            Wander();
    }

    private void Wander()
    {
        Vector3 movement = (m_TargetPosition - transform.position).normalized;
        transform.position += movement * Time.deltaTime * Speed;
    }

    private Vector3 SetNewTarget()
    {
        Vector3 v3 = Vector3.zero;
        bool isValid = false;
        while (!isValid)
        {
            float angle = Random.value * 2 * Mathf.PI;
            var v = new Vector3(Mathf.Cos(angle), transform.position.y, Mathf.Sin(angle));
            v.x *= 10;
            v.z *= 10;

            if ((v - transform.position).magnitude > Speed * m_WanderTime + 1)
            {
                v3 = v;
                isValid = true;
            }
        }

        return v3;
    }

    private void CheckSurroundings(int _numRays, float _rayLength)
    {
        List<Vector3> hitsDirections = new List<Vector3>();
        for (int i = 0; i < _numRays; i++)
        {
            float angle = 2 * Mathf.PI / _numRays;
            angle = i * angle + angle / 2;
            Vector3 direction = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));

            Ray ray = new Ray(transform.position + direction * Mathf.Sqrt(2) / 2, direction);
            if (Physics.Raycast(ray, _rayLength))
            {
                hitsDirections.Add(direction);
                Debug.DrawRay(transform.position + direction * Mathf.Sqrt(2) / 2, direction * _rayLength, Color.green);
            }
            else
            {
                Debug.DrawRay(transform.position + direction * Mathf.Sqrt(2)/2, direction * _rayLength, Color.red);
            }
        }

        Vector3? resultDirection = Vectors3Addition(hitsDirections);
        if (resultDirection.HasValue)
        {
            resultDirection *= -1;
            m_TargetPosition = transform.position + resultDirection.Value.normalized * (Speed * (m_WanderTime - m_CurrentTime) + 1);
            Debug.DrawRay(transform.position + resultDirection.Value.normalized * Mathf.Sqrt(2) / 2, resultDirection.Value * _rayLength, Color.blue);
        }
    }

    private Vector3? Vectors3Addition(List<Vector3> hitsDirections)
    {
        Vector3? sum = null;
        foreach (Vector3 v in hitsDirections)
        {
            sum = (sum == null) ? Vector3.zero : sum;
            sum += v;
        }
        return sum;
    }

    private float SetNewWanderTime()
    {
        return Random.Range(m_MinWanderTime, m_MaxWanderTime);
    }
}