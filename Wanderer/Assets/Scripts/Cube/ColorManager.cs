﻿using UnityEngine;

public class ColorManager : MonoBehaviour
{
    [SerializeField] MaterialStruct[] m_Materials;
    MaterialStruct m_MaterialStruct;

    void Start()
    {
        m_MaterialStruct = WeightedMaterialChoice(m_Materials);
        GetComponent<MeshRenderer>().material = m_MaterialStruct.material;
        transform.name = m_MaterialStruct.name;
    }

    MaterialStruct WeightedMaterialChoice(MaterialStruct[] _materials)
    {
        MaterialStruct mat = new MaterialStruct();

        // List of the cumulative sum of weights
        float[] sumOfChances = new float[_materials.Length];

        // Fill the list + handling the case with only 1 weight
        sumOfChances[0] = _materials[0].density;
        if (_materials.Length > 1)
        {
            for (int i = 1; i < _materials.Length; i++)
            {
                sumOfChances[i] = sumOfChances[i - 1] + _materials[i].density;
            }

            // Pick a random number between 0 and the total sum of weights
            float r = Random.value * sumOfChances[_materials.Length - 1];

            // Find the index of the last item whose cumulative sum is above the random number
            // Pick the asset at index and return it
            for (int i = 0; i < _materials.Length; i++)
            {
                if (r <= sumOfChances[i])
                {
                    mat = _materials[i];
                    break;
                }
            }
        }
        else // In case there is only 1 item in the list
        {
            mat = _materials[0];
        }

        return mat;
    }
}

[System.Serializable]
public struct MaterialStruct
{
    public string name;
    public Material material;
    public float density;
}