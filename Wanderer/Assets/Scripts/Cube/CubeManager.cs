﻿using UnityEngine;

public class CubeManager : MonoBehaviour
{
    [SerializeField] private GameObject m_CubePrefab;

    [Header("Spawn limiters:")]
    [SerializeField] private float m_XMin = -9.0f;
    [SerializeField] private float m_XMax = 16.0f;
    [SerializeField] private float m_ZMin = -9.0f;
    [SerializeField] private float m_ZMax = 9.0f;
    [SerializeField] private float m_SecurityDistance = 1.0f;
    [SerializeField] private int m_MaxIterations = 10;

    private void Start()
    {
        GameManager.Instance.OnPopulationChanged += AdjustPopulation;
        GameManager.Instance.OnSpeedChanged += AdjustSpeed;
        GameManager.Instance.OnAwarenessChanged += AdjustAwareness;
        GameManager.Instance.OnProximityChanged += AdjustProximity;
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnPopulationChanged -= AdjustPopulation;
        GameManager.Instance.OnSpeedChanged -= AdjustSpeed;
        GameManager.Instance.OnAwarenessChanged -= AdjustAwareness;
        GameManager.Instance.OnProximityChanged -= AdjustProximity;
    }

    private void AdjustPopulation(int population)
    {
        if (GameManager.Instance.Population > GetAllCubes().Length)
        {
            Instantiate(m_CubePrefab, RandomPosition(), Quaternion.identity, transform);
        }
        else if (GameManager.Instance.Population < GetAllCubes().Length)
        {
            Destroy(GameObject.FindGameObjectWithTag("Cube"));
        }
    }

    private void AdjustSpeed(float speed)
    {
        foreach (GameObject cube in GetAllCubes())
        {
            cube.GetComponent<Cube>().Speed = GameManager.Instance.Speed;
        }
    }

    private void AdjustAwareness(int awareness)
    {
        foreach (GameObject cube in GetAllCubes())
        {
            cube.GetComponent<Cube>().NumRays = GameManager.Instance.Awareness;
        }
    }

    private void AdjustProximity(float proximity)
    {
        foreach (GameObject cube in GetAllCubes())
        {
            cube.GetComponent<Cube>().RayLength = GameManager.Instance.Proximity;
        }
    }

    private GameObject[] GetAllCubes()
    {
        return GameObject.FindGameObjectsWithTag("Cube");
    }

    private Vector3 RandomPosition()
    {
        GameObject[] cubes = GetAllCubes();

        bool hasFound = false; int i = 0;
        while (!hasFound)
        {
            Vector3 v = new Vector3(Random.Range(m_XMin + 1, m_XMax - 1), 0, Random.Range(m_ZMin + 1, m_ZMax - 1));

            bool isValid = true;

            foreach (GameObject cube in cubes)
            {
                if ((v - cube.transform.position).magnitude < m_SecurityDistance)
                    isValid = false;
            }

            if (isValid)
                return v;

            i++;
            if (i > m_MaxIterations)
                hasFound = true;
        }

        return Vector3.zero;
    }
}