﻿using UnityEngine;
using TMPro;

public class UIProximityText : MonoBehaviour, IUpdateTextFromFloat
{
    TextMeshProUGUI m_Text;

    void Start()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
        GameManager.Instance.OnProximityChanged += UpdateText;
    }

    public void UpdateText(float f)
    {
        var speed = Mathf.FloorToInt(10.0f * GameManager.Instance.Proximity) / 10.0f;
        m_Text.text = "PROXIMITY: " + speed.ToString();
    }
}