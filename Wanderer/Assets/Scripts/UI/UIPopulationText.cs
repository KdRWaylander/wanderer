﻿using UnityEngine;
using TMPro;

public class UIPopulationText : MonoBehaviour, IUpdateTextFromInt
{
    TextMeshProUGUI m_Text;

    void Start()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
        GameManager.Instance.OnPopulationChanged += UpdateText;
    }

    public void UpdateText(int _population)
    {
        m_Text.text = "POPULATION: " + GameManager.Instance.Population.ToString();
    }
}