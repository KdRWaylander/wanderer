﻿using UnityEngine;
using UnityEngine.UI;

public class UIInterface : MonoBehaviour
{
    [SerializeField] Text m_SoundText;

    [Header("Steps")]
    [SerializeField] private int m_PopulationStep = 1;
    [SerializeField] private float m_SpeedStep = 0.1f;
    [SerializeField] private int m_AwarenessStep = 1;
    [SerializeField] private float m_ProximityStep = 0.1f;

    public void AddPopulation()
    {
        if (GameManager.Instance.Population < GameManager.Instance.MaxPopulation)
        {
            GameManager.Instance.Population += m_PopulationStep;
            GameManager.Instance.ChangePopulation();
        }
    }

    public void WithdrawPopulation()
    {
        if (GameManager.Instance.Population > GameManager.Instance.MinPopulation)
        {
            GameManager.Instance.Population -= m_PopulationStep;
            GameManager.Instance.ChangePopulation();
        }
    }

    public void AddSpeed()
    {
        if (GameManager.Instance.Speed < GameManager.Instance.MaxSpeed)
        {
            GameManager.Instance.Speed += m_SpeedStep;
            GameManager.Instance.ChangeSpeed();
        }
    }

    public void WithdrawSpeed()
    {
        if (GameManager.Instance.Speed > GameManager.Instance.MinSpeed)
        {
            GameManager.Instance.Speed -= m_SpeedStep;
            GameManager.Instance.ChangeSpeed();
        }
    }

    public void AddAwareness()
    {
        if (GameManager.Instance.Awareness < GameManager.Instance.MaxAwareness)
        {
            GameManager.Instance.Awareness += m_AwarenessStep;
            GameManager.Instance.ChangeAwareness();
        }
    }

    public void WithdrawAwareness()
    {
        if (GameManager.Instance.Awareness > GameManager.Instance.MinAwareness)
        {
            GameManager.Instance.Awareness -= m_AwarenessStep;
            GameManager.Instance.ChangeAwareness();
        }
    }

    public void AddProximity()
    {
        if (GameManager.Instance.Proximity < GameManager.Instance.MaxProximity)
        {
            GameManager.Instance.Proximity += m_ProximityStep;
            GameManager.Instance.ChangeProximity();
        }
    }

    public void WithdrawProximity()
    {
        if (GameManager.Instance.Proximity > GameManager.Instance.MinProximity)
        {
            GameManager.Instance.Proximity -= m_ProximityStep;
            GameManager.Instance.ChangeProximity();
        }
    }

    public void Sound()
    {
        FindObjectOfType<AudioSource>().mute = !FindObjectOfType<AudioSource>().mute;

        if (!FindObjectOfType<AudioSource>().mute)
            m_SoundText.text = "SOUND: OK";
        else
            m_SoundText.text = "SOUND: KO";
    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quit !");
    }
}