﻿using TMPro;
using UnityEngine;

public class UIAwarenessText : MonoBehaviour, IUpdateTextFromInt
{
    TextMeshProUGUI m_Text;

    void Start()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
        GameManager.Instance.OnAwarenessChanged += UpdateText;
    }

    public void UpdateText(int i)
    {
        m_Text.text = "AWARENESS: " + GameManager.Instance.Awareness.ToString();
    }
}