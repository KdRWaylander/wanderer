﻿using TMPro;
using UnityEngine;

public class UISpeedText : MonoBehaviour, IUpdateTextFromFloat
{
    TextMeshProUGUI m_Text;

    void Start()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
        GameManager.Instance.OnSpeedChanged += UpdateText;
    }

    public void UpdateText(float f)
    {
        var speed = Mathf.FloorToInt(10.0f * GameManager.Instance.Speed) / 10.0f;
        m_Text.text = "SPEED: " + speed.ToString();
    }
}