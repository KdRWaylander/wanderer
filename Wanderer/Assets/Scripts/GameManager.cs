﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int Population { get; set; }
    public float Speed { get; set; }
    public int Awareness { get; set; }
    public float Proximity { get; set; }

    public int MaxPopulation { get; private set; }
    public int MinPopulation { get; private set; }
    public float MaxSpeed { get; private set; }
    public float MinSpeed { get; private set; }
    public int MaxAwareness { get; private set; }
    public int MinAwareness { get; private set; }
    public float MaxProximity { get; private set; }
    public float MinProximity { get; private set; }

    public event Action<int> OnPopulationChanged;
    public event Action<float> OnSpeedChanged;
    public event Action<int> OnAwarenessChanged;
    public event Action<float> OnProximityChanged;

    private void Awake()
    {
        Instance = this;

        Population = 12;
        Speed = 2;
        Awareness = 16;
        Proximity = 0.2f;

        MaxPopulation = 20;
        MinPopulation = 0;
        MaxSpeed = 5.0f;
        MinSpeed = 0.0f;
        MaxAwareness = 32;
        MinAwareness = 3;
        MaxProximity = 1.0f;
        MinProximity = 0.1f;
    }

    private void Start()
    {
        ChangePopulation();
        ChangeSpeed();
        ChangeAwareness();
        ChangeProximity();
    }

    public void ChangePopulation()
    {
        if(OnPopulationChanged != null)
            OnPopulationChanged(Population);
    }

    public void ChangeSpeed()
    {
        if (OnSpeedChanged != null)
            OnSpeedChanged(Speed);
    }

    public void ChangeAwareness()
    {
        if (OnAwarenessChanged != null)
            OnAwarenessChanged(Awareness);
    }

    public void ChangeProximity()
    {
        if (OnProximityChanged != null)
            OnProximityChanged(Proximity);
    }
}